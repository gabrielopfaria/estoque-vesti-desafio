<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    use HasFactory;
    protected $table = 'historicos';
    protected $fillable = [
        'produto_id',
        'descricao',
        'user_id',
    ];
}
