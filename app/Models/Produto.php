<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    use HasFactory;
    protected $table = 'produtos';
    protected $fillable = [
        'codigo',
        'categoria',
        'nome',
        'preco',
        'tamanho',
        'quantidade',
        'composicao',
        'imagem_1',
        'imagem_2',
        'imagem_3'
    ];
}
