<?php

namespace App\Http\Controllers;

use App\Models\Historico;
use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Produto::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'codigo' => 'required',
            'categoria' => 'required',
            'nome' => 'required',
            'preco' => 'required',
            'tamanho' => 'required',
            'quantidade' => 'required',
            'composicao' => 'required',
            'imagem_1' => 'required',
        ]);

        $user = Auth::user()->name;
        $produto = Produto::create($request->all());
        $historico = Historico::create([
            'produto_id' => $produto->id,
            'user_id' => Auth::user()->id,
            'descricao' => "Produto {$produto->nome} cadastrado por {$user}"
        ]);

        return $produto;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function show(Produto $produto)
    {
        $produto = Produto::find($produto->id);
        $historico = Historico::where('produto_id', 'like', $produto->id)->get();
        return [$produto,$historico];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function edit(Produto $produto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produto $produto)
    {
        $user = Auth::user()->name;
        $descricao = "";
        $produto = Produto::find($produto->id);
        $produto_atual = $request->all();

        if (isset($produto_atual['codigo']) && $produto->codigo != $produto_atual['codigo']) {
            $descricao .= "O Usuário {$user}, alterou o código do produto de {$produto->codigo} para {$produto_atual['codigo']} | ";
        }
        if (isset($produto_atual['categoria']) && $produto->categoria != $produto_atual['categoria']) {
            $descricao .= "O Usuário {$user}, alterou a categoria do produto de {$produto->categoria} para {$produto_atual['categoria']} | ";
        }
        if (isset($produto_atual['nome']) && $produto->nome != $produto_atual['nome']) {
            $descricao .= "O Usuário {$user}, alterou o nome do produto de {$produto->nome} para {$produto_atual['nome']} | ";
        }
        if (isset($produto_atual['preco']) && $produto->preco != $produto_atual['preco']) {
            $descricao .= "O Usuário {$user}, alterou o preço do produto de {$produto->preco} para {$produto_atual['preco']} | ";
        }
        if (isset($produto_atual['tamanho']) && $produto->tamanho != $produto_atual['tamanho']) {
            $descricao .= "O Usuário {$user}, alterou o tamanho do produto de {$produto->tamanho} para {$produto_atual['tamanho']} | ";
        }
        if (isset($produto_atual['quantidade']) && $produto->quantidade != $produto_atual['quantidade']) {
            $descricao .= "O Usuário {$user}, alterou a quantidade do produto de {$produto->quantidade} para {$produto_atual['quantidade']} | ";
        }
        if (isset($produto_atual['imagem_1']) && $produto->imagem_1 != $produto_atual['imagem_1']) {
            $descricao .= "O Usuário {$user}, alterou a Imagem do produto de {$produto->imagem_1} para {$produto_atual['imagem_1']} | ";
        }
        if (isset($produto_atual['imagem_2']) && $produto->imagem_2 != $produto_atual['imagem_2']) {
            $descricao .= "O Usuário {$user}, alterou a Imagem do produto de {$produto->imagem_2} para {$produto_atual['imagem_2']} | ";
        }
        if (isset($produto_atual['imagem_3']) && $produto->imagem_3 != $produto_atual['imagem_3']) {
            $descricao .= "O Usuário {$user}, alterou a Imagem do produto de {$produto->imagem_3} para {$produto_atual['imagem_3']} | ";
        }
        if (isset($produto_atual['composicao']) && $produto->composicao != $produto_atual['composicao']) {
            $descricao .= "O Usuário {$user}, alterou a Composição do produto de {$produto->composicao} para {$produto_atual['composicao']} | ";
        }
        if ($descricao != null) {
            Historico::create([
                'produto_id' => $produto->id,
                'user_id' => Auth::user()->id,
                'descricao' => $descricao
            ]);
        }
        $produto->update($request->all());
        return $produto;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produto $produto)
    {
        $user = Auth::user()->name;
        Historico::create([
            'produto_id' => $produto->id,
            'user_id' => Auth::user()->id,
            'descricao' => "O usuário {$user} excluiu o produto {$produto->nome}"
        ]);
        return Produto::destroy($produto->id);
    }
}
