# Desafio php vesti 
## Api para estoque da loja do João

### Configuração do projeto
1º Realizar o download do projeto;
2º Entrar na pasta do projeto pelo termina e rodar o comando "composer install" em seu terminal;
3º Realizar uma cópia do arquivo .env.example o renomeando para .env, configura em .env a conexão de dados com o banco de sua preferência;
4º Rodar o comando php artisan key:generate para criar a chave da aplicação;
5º Rodar o comando php artisan serve;
5º Rodar o comando php artisan migrate;

#### Iniciar o ambiente de servidor
Utilize o comando php artisan serve

#### Utilizando a API
1º Novo usuário
http://localhost:8000/api/register
Usar o método POST, enviar os seguintes parâmetros: nome, email, password, password_confirmation

2º Login do usuário
http://localhost:8000/api/login
Usar o método POST, enviar os seguintes parâmetros: email, password

--------------------------------------------------------
## Importante
#### Todas as rotas de CRUD necessitam do token. O token deve ser informado via bearer token
--------------------------------------------------------

# Rota para cadastrar produto
http://localhost:8000/api/produtos
Usar método POST para cadastro de produtos;
Parâmetros que devem ser usados para cadastro de produtos:
codigo;
categoria;
nome;
preco;
tamanho;
quantidade;
composicao;
imagem_1;
imagem_2;
imagem_3;
Todos os campos são obrigatórios exceto imagem_2 e imagem_3 que podem ser nulos de acordo com a necessidade.

# Rota para buscar produto
http://localhost:8000/api/produtos/{id}
Usar o método GET para buscar produtos
Enviar o ID do produto e irá retornar os dados do produto + histórico de operações realizadas.

# Rota para editar o produto
http://localhost:8000/api/produtos/{id}
Usar o método PUT para editar os produtos

# Rota para excluir o produto
http://localhost:8000/api/produtos/{id}
Enviar id do produto o histórico do mesmo parmecerá para que possa ver quem excluiu o produto.

--------------------------------------------------------
# Rota para listar todos os históricos
http://localhost:8000/api/historico

# Rota para listar histórico de um produto específico
http://localhost:8000/api/historico/{id}
#### Observação o ID informado deve ser do produto

No histórico é salvo cada detalhe do que foi feito com o produto informando o usuário que realizou a operação.

#Docker
Para realizar a instalação via docker, alterar as portas no arquivo docker-compose.yml conforme listado no comentário e rodar no terminal o comando:
docker-composer up -d
